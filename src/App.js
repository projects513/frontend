import React, { useState, useEffect } from 'react';
import './App.css';
import Dropdown from 'react-dropdown'
import './dropDownStyle.css'
import gerb from './images/gerb.png';
import gerbBlack from './images/gerbBlack.png';
import question from './images/question.png';
import 'react-activity-indicator/src/activityindicator.css'

const url = "http://api.project513.loc";


const Page = (props) => {
  const [inited, setInited] = useState(false);
  const [data, setData] = useState(null);
  const [regions, setRegions] = useState([]);
  const [transport, setTransport] = useState([]);
  const [result, setResult] = useState(null);
  const [showAdditionalOptions, setShowAdditionalOptions] = useState(false);

  const [fuelNorm, setFuelNorm] = useState('');
  const [correctionCoef, setCorrectionCoef] = useState('');
  const [heatingNorm, setHeatingNorm] = useState('');
  const [velocity, setVelocity] = useState('');
  const [month, setMonth] = useState('');
  const [selectedRegion, setSelectedRegion] = useState(
    {
      "id": "39",
      "title": "Калининградская область",
      "group_id": 1
    });
  const [selectedTransport, setSelectedTransport] = useState(null);


  const fetchData = () => {
    return fetch(url + '/coefficients', {
      method: 'get'
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        setData(data);
      })
      .catch(error => console.log(error.message));
  };

  const fetchRegions = (callback) => {
    return fetch(url + '/regions', {
      method: 'get'
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        callback(data);
      })
      .catch(error => console.log(error.message));
  };

  const fetchTransport = (callback) => {
    return fetch(url + '/transport_types', {
      method: 'get',
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        callback(data);
      })
      .catch(error => console.log(error.message));
  };


  const postCalculations = (params) => {
    return fetch(url + '/calculate', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(params)
    })
      .then(response => {
        return response.json()
      })
      .then(result => {
        console.log("!!! " + result.result)
        setResult(result.result);
      })
      .catch(error => console.log(error.message));
  };

  if (!inited) {
    setInited(true);
    fetchRegions(data => setRegions(data));
    fetchTransport(data => setTransport(data));
    fetchData(data => setData(data));
  }

  const addZeros = (count) => {
    return count === 0 ? '00' : count < 10 ? '0' + count : count;
  }

  return (
    <div>
      <div className='headerStyle'>
        <div className='headerContent'>
          <img src={gerb} alt="Logo" />
        </div>
        <label className='headerLabelBold'>513.gov.ru</label>
        <label className='subHeaderLabelBold'>Рассчетный сервис</label>
      </div>
      <div style={{ marginLeft: '171pt' }}>
        <div>
          <label className='h1'>Рассчет стоимости контракта</label>
        </div>
        <div>
          <label className='h2'>Основные параметры</label>
        </div>
        <div style={{ marginTop: '34pt', marginBottom: '12pt', width: '400pt' }}>
          <label className='titleStyle'>Регион</label>
        </div>
        <div>
          <Dropdown
            style={{ width: '400pt', height: '50pt' }}
            fluid
            multiple
            search
            selection
            options={regions.map(item => item.title)}
            onChange={
              (item) => {
                const regionInfo = regions.find(el => el.title === item.value);
                if (regionInfo != null) {
                  setSelectedRegion(regionInfo);
                }
              }
            }
            value={selectedRegion.title}
            placeholder="Выберите значение"
          />
        </div>
        <div style={{ marginTop: '34pt', marginBottom: '12pt', width: '400pt' }}>
          <label className='titleStyle'>Тип транспорта</label>
        </div>
        <div>
          <Dropdown
            style={{ width: '400pt', height: '50pt' }}
            fluid
            multiple
            search
            selection
            options={transport.map(item => item.title)}
            onChange={(item) => {
              const info = transport.find(el => el.title === item.value);
              if (info != null) {
                setSelectedTransport(info);
              }
            }}
            value={selectedTransport ? selectedTransport.title : ''}
            placeholder="Выберите значение"
          />
        </div>

        <div>
          <div style={{ marginTop: '34pt', marginBottom: '12pt', width: '400pt' }}>
            <label className='titleStyle'>Транспортная норма расхода топлива</label>
            <img className='question_label' src={question} alt="Logo" />
          </div>
          <div>
            <input className="inputStyle" autoComplete="sdfasfda" placeholder='Введите значение' value={fuelNorm} onChange={e => setFuelNorm(e.target.value)} type="text" name="name" />
          </div>
        </div>
        <div>
          <div style={{ marginTop: '34pt', marginBottom: '12pt', width: '400pt' }}>
            <label className='titleStyle'>Поправочный коэффициент к норме</label>
            <img className='question_label' src={question} alt="Logo" />
          </div>
          <div>
            <input className="inputStyle" autoComplete="adfasdf" placeholder='Введите значение' value={correctionCoef} onChange={e => setCorrectionCoef(e.target.value)} type="text" name="name" />
          </div>
        </div>
        <div>
          <div style={{ marginTop: '34pt', marginBottom: '12pt', width: '400pt' }}>
            <label className='titleStyle'>Норма расхода топлива на отопление салона</label>
          </div>
          <div>
            <input className="inputStyle" autoComplete="wwwwe" placeholder='Введите значение' value={heatingNorm} onChange={e => setHeatingNorm(e.target.value)} type="text" name="name" />
          </div>
        </div>
        <div>
          <div style={{ marginTop: '34pt', marginBottom: '12pt', width: '400pt' }}>
            <label className='titleStyle'>Эксплуатационная скорость</label>
            <img className='question_label' src={question} alt="Logo" />
          </div>
          <div>
            <input className="inputStyle" autoComplete="nope" placeholder='Введите значение' value={month} onChange={e => setMonth(e.target.value)} type="text" name="name" />
          </div>
        </div>
        <div>
          <div style={{ marginTop: '34pt', marginBottom: '12pt', width: '400pt' }}>
            <label className='titleStyle'>Срок работы отопителей салона</label>
            <img className='question_label' src={question} alt="Logo" />
          </div>
          <div>
            <input className="inputStyle" autoComplete="werfcdasd" placeholder='Введите значение' value={velocity} onChange={e => setVelocity(e.target.value)} type="text" name="name" />
          </div>
        </div>
        

        <div>
          <button className="button" onClick={() => postCalculations({
            'region_id': selectedRegion.id,
            'transport_type': parseInt(selectedTransport.id),
            'fuel_norm': parseFloat(fuelNorm),
            'correction_coef': parseFloat(correctionCoef),
            'heating_norm': parseFloat(heatingNorm),
            'velocity': parseFloat(velocity),
            'months': parseInt(month)
          })}><label className='buttonLabel'>Рассчитать</label></button>
        </div>
        {result &&
          <div>
            <div style={{ width: '600pt' }}>
              <div>
              <label class='resultDescription'>{'Максимальная себестоимость 1 км пробега транспортных'}</label>
              </div>
              <label class='resultDescription'>{'средств i-го класса в t-ый год срока действия контракта'}</label>
              <label class='resultDescriptionBold'>{' (S'}</label>
              <label class='resultDescriptionBoldFloat'>{'ti'}</label>
              <label class='resultDescriptionBold'>{')'}</label>
            </div>
            <div style={{marginTop: '20pt', marginBottom: '64pt'}}>
              <label class='resultStyle'>
                {Math.floor(result) + ','}
                <label class='resultPartialStyle'>{addZeros(Math.round(100 * (result - Math.floor(result))))}</label>
                <label class='resultStyle' style={{marginLeft: '6pt'}}>₽</label>
              </label>
            </div>
          </div>
        }
      </div>
      <div className='line'></div>
      <div className='footerStyle'>
        <div className='footerContent'>
          <img src={gerbBlack} alt="Logo" />
        </div>
        <label className='footerLabelBold'>513.gov.ru</label>
        <label className='subFooterLabelBold'>Рассчетный сервис</label>
      </div>
    </div>
  );
}

const App = (props) => <Page />
export default App;